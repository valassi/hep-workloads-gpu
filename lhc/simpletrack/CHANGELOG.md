# QA

FIXES:
* For unpriviledged Singularity runs switched to /tmp/jobs as output folder. 

# Master

UPDATES:
* intel: NEO version updated to 20.25.17111 and oneAPI DPC++ to 2020.10.6.0.4 (June releases).
* ROCm container added

CHANGES:
* Tagged build based on the spec definition.
* Using trigger-based build to rebuild only on simpletrack changes.
* CI/CD basic functionality test added for the CPU-based container builds i.e. intel and pocl

FEATURES:
* Switched to argument for benchmark setup instead of environment variables.
* Added "benchmark" mode to run and generate json output for the runs.
* Generate yaml alongside the json summary.
* Standalone execution of the simpletrack benchmark without an orchestrator.

FIXES:
* ocl-icd-dev package explicitely installed now to avoid build failures.
* Using simpletrack device lists instead of clinfo.

