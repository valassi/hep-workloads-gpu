#!/bin/bash

set -e

PARTICLES=20000
TURNS=15
DEVICE="0.0"
BENCHMARK="device"

#################
get_info()   { echo "$1" | grep "$2" | cut -d":" -f2 | head -n $3 | tail -1 | xargs; }
#################

#################
get_devices() {
#################
  info="$(python3 benchmark_opencl.py -s)"
  devices=`echo "$info" | grep "Device" | cut -d"'" -f2`
 
  for dev_id in $(seq 1 $(echo "$devices" | wc -l))
  do
    echo "\"device_id\":\"$(echo "$info" | grep "Device" | cut -d"'" -f2 | head -n $dev_id | tail -1 | xargs)\",\"name\":\"$(get_info "$info" "Device" "$dev_id")\",\"platform\":\"$(get_info "$info" "Platform" "$dev_id")\""
  done
}

#################
log() {
#################
  case $1 in
    error)  shift 1; echo -e "\e[31m>>> ERROR:\e[0m $*\n" | tee -a $WORK_DIR/out.log ; exit 2 ;;
    info)   shift 1; echo -e "\e[34m$*\e[0m\n" | tee -a $WORK_DIR/out.log ;;
    silent) shift 1; echo "$*" >> $WORK_DIR/out.log ;;
    *)      echo "$*" | tee -a $WORK_DIR/out.log ;
  esac
}

#################
parse_args() {
#################
  while [ $# -gt 0 ]
  do
    case $1 in
      --help|-help|-h)
        echo -e "Usage: $0 <options>\n\nOptions:\n\t-p, --particles\t\tNumber of particles. Default: $PARTICLES\n\t-t, --turns\t\tNumber of turns. Default: $TURNS\n\t-d, --device\t\tSpecify the device to run on. Default: $DEVICE\n\t-b, --benchmark\t\tBenchmark modes: all or device. Default: $BENCHMARK\n\t-s, --show\t\tList all available devices and exit\n\t-h, --help\t\tPrints this message\n"; exit 0;;
      --particles|-p) shift 1 ; PARTICLES=$1 ;;
      --turns|-t)     shift 1 ; TURNS=$1 ;;
      --device|-d)    shift 1 ; DEVICE=$1 ;;
      --benchmark|-b) shift 1 ; BENCHMARK=$1 ;;
      --show|-s)      python3 benchmark_opencl.py -s ; exit 0 ;;
      *) log error "Invalid argument: $*. Please use: $0 --help for the list of available options"
    esac
    shift 1
  done
}

#################
run_benchmark() {
#################
  OUT=$(python3 benchmark_opencl.py -p $PARTICLES -t $TURNS -d $1 2>&1 | tee -a $WORK_DIR/out.log)
  RESULT=`echo "$OUT" | grep 'particles\*turns/seconds'` 

  if [ "$RESULT" == "" ]; then log error "Failed to parse the output. Failed run?";
  else echo $(echo $RESULT | cut -d' ' -f1 | xargs); fi
}

#################
get_json() {
#################
  dev_line=`echo "$DEVICES" | grep "\"device_id\":\"$1\""`
  echo "{\"copies\":\"1\",\"threads_per_copy\":\"$TURNS\",\"events_per_thread\":\"$PARTICLES\",\"wl-scores\":{\"simpletrack\":\"$2\"},\"device\":{$dev_line}}"
}

###################################
####### Main ######################

if [ ! "$CI_PROJECT_DIR" == "" ]; then WORK_DIR=$CI_PROJECT_DIR/jobs; else WORK_DIR="/tmp/jobs"; fi

if [ ! -d $WORK_DIR ]; then mkdir -p $WORK_DIR; fi
if [ -f $WORK_DIR/out.log ]; then rm $WORK_DIR/out.log; fi

parse_args $*

CLINFO_OUT=`echo "$(clinfo | tee $WORK_DIR/clinfo.log)"`

if [ `echo $CLINFO_OUT | grep 'Number of platforms' | awk '{print $4}'` -eq 0 ]; then 
	log error "No platforms found. Exiting.."; fi

DEVICES="$(get_devices | tee $WORK_DIR/devices.log)"
log silent "Devices found:\n$DEVICES"

case $BENCHMARK in
  all)     list=( `echo "$DEVICES" | cut -d"\"" -f4 | xargs` );
	   log info "Running benchmark for ${#list[@]} device(s)"; 
	   for dev in "${list[@]}"
	   do
		   OUT=$(run_benchmark $dev); get_json $dev $OUT | tee -a $WORK_DIR/summary.json;
	   done ;;

  device)  log info "Running benchmark for $DEVICE device";
	   OUT=$(run_benchmark $DEVICE); get_json $DEVICE $OUT | tee -a $WORK_DIR/summary.json ;;
  *)       python3 benchmark_opencl.py -p $PARTICLES -t $TURNS -d $DEVICE 2>&1 | tee -a $WORK_DIR/out.log;	  
esac

log info "\nFinished running simpletrack"
