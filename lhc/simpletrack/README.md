# Overview

Docker images containing OpenCL-oriented Simpletrack benchmark built for a selection of GPU and CPU platforms:

- __Intel__: contains [GPU NEO runtime](https://github.com/intel/compute-runtime) and OneAPI version i.e. DPC++ and SYCL support contains only the [OpenCL CPU runtimes](https://software.intel.com/content/www/us/en/develop/articles/opencl-drivers.html).
- __ROCm__: AMD Radeon Open Ecosystem platform runtimes installed using [these](https://rocmdocs.amd.com/en/latest/) instructions.
- __Nvidia__: contains OpenCL runtime built using [this](https://nvidia.github.io/nvidia-container-runtime/) repository.
- __POCL__: Portable Computing Language implementation built with this [repo](https://github.com/pocl/pocl)

|              | __intel__   | __rocm__  | __nvidia__ | __pocl__   |
|--------------|:-----------:|:-----------:|:--------:|:----------:|
| __GPU__      | :heavy_check_mark:  | :heavy_check_mark: | :heavy_check_mark: |        |       
| __CPU__      | :heavy_check_mark:  |                    |                    | :heavy_check_mark: |

# Usage

Default entrypoint provides the following options that can be used, for example:
```
~$ docker run --rm gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-gpu/simpletrack/intel:latest -h

Usage: ./lhc-simpletrack.sh <options>

Options:
	-p, --particles		Number of particles. Default: 20000
	-t, --turns		Number of turns. Default: 15
	-d, --device		Specify the device to run on. Default: 0.0
	-b, --benchmark		Benchmark modes: all or device. Default: device
	-s, --show		List all available devices and exit
        -h, --help              Prints this message
```

- To run the container for 1000 particles, 5 particles for device "1.0": ```~$ docker run --rm <image:tag> -p 1000 -t 5 -d "1.0"```
- Use the benchmark option "all" to execute runs on all available devices: ```~$ docker run --rm <image:tag> -b "all"```
- To discover available platforms use the show option: ```~$ docker run --rm <image:tag> -s```

The benchmark mode allows to generate logs and output files in a default location (/tmp/jobs or $CI_PROJECT_DIR) for either a single or all available devices.

## GPU Passthrough

To passthrough the device to the container, use the following options:

| Target      | Docker             | Singularity |
|:------------|:-------------------|:------------|
| __Nvidia__  | ```--gpus all```   | ```--nv```  |
| __AMD__     | ```--device /dev/kfd --device /dev/dri``` | ```--rocm``` |
| __Intel__   | ```--device /dev/dri``` | |

