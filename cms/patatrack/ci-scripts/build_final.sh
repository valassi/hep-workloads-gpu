#!/bin/bash

set -x
set -e

# First move all folders in the right place
date
mv /stage/cvmfs /cvmfs

date
mv /stage/cms-patatrack /bmk/./cms-patatrack

# Make only readable
date
chmod -R 555 /cvmfs

# FIXME This checksum takes a lot of time.
# Commenting it. Can be substituted by a checksum using cvmfs utilities
#tar -cf /tmp/cvmfs_checksum.tar /cvmfs && md5sum /tmp/cvmfs_checksum.tar | cut -f1 -d" " > /tmp/cvmfs_checksum && rm /tmp/cvmfs_checksum.tar

# Checksum code in orchestrator dir.
# This MUST happen before linking the data dir
# otherwise will take a lot of time to tar
date
tar -cf /tmp/bmk_checksum.tar /bmk --exclude='data' && md5sum /tmp/bmk_checksum.tar | cut -f1 -d" " >/tmp/bmk_checksum && rm /tmp/bmk_checksum.tar #FIXME

# The data dir has already a checksum in /tmp/bmkdata_checksum
# generated in nvidia.Dockerfile.1
date
if [ ! -d /bmk/./cms-patatrack/data ]; then
    mkdir /bmk/./cms-patatrack/data
fi
for file in $(ls /bmk/data); do
    ln -sf /bmk/data/$file /bmk/./cms-patatrack/data/$file
done

date
cvmfs_checksum=$(cat /tmp/cvmfs_checksum || echo "NotAvailable") 
bmkdata_checksum=$(cat /tmp/bmkdata_checksum || echo "NotAvailable") 
bmk_checksum=$(cat /tmp/bmk_checksum || echo "NotAvailable") 
echo '{"version":"v1.3","description":"CMS RECO of ttbar events, based on CMSSW_10_2_9","cvmfs_checksum":"'$cvmfs_checksum'","bmkdata_checksum":"'$bmkdata_checksum'","bmk_checksum":"'$bmk_checksum'"}' >/bmk/./cms-patatrack/version.json #FIXME

# Add user 'bmkuser' to benchmarks as a non-root user (BMK-166 and BMK-167)
# shoudl not be needed, using cvmfs read only
#groupadd bmkuser
#useradd -g bmkuser --create-home --shell /bin/bash bmkuser
