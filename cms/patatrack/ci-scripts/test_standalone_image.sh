#!/bin/bash

# script used in gitlab CI
# for job job_test_standalone_image
# in file cms/cms-patatrack-ci.yml

function _script() {
    docker pull ${IMAGE_NAME}
    # Here comes the test run of the CMS Patatrack standalone container. Arguments are for the time being defaults/hardcoded FIXME
    docker run --rm --gpus '"device=0"' -v ${RESULTS_DIR}:/results ${IMAGE_NAME} -e 100 -t 8 -c 1
    mv ${RESULTS_DIR} ${CI_PROJECT_DIR}/.
}

export RESULTS_DIR=/scratch/results/CI-JOB-${CI_JOB_ID}
export IMAGE_NAME=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-gpu/cms/cms-patatrack-nvidia-bmk:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
