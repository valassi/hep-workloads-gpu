#!/bin/env bash
# Wrapper script based on work from https://github.com/sciaba/patatrack-tests
# 2020.06 David Southwick <david.southwick@cern.ch> - include newer workflow for pre8 patatrack, singularity support

#set -x # enable debug printouts

set -e # immediate exit on error


function myecho(){
  echo -e "[${FUNCNAME[1]}] $@"
}

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then myecho "ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  myecho "($1) $(date) starting in $(pwd)"
  # Extra CMS-PATATRACK-specific setup
  
  #######################################
  # This needs to be fixed

  myecho "current dir is `pwd`"
  myecho "files in `pwd` are"
  ls -l
  ${BMKDIR}/utility_scripts/benchmark.py ${BMKDIR}/cmssw_config.py #>>$LOG 2>&1 3>&1
  #######################################

  status=${?}
  myecho "($1) $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# FIXME
# Using validateInputArguments for another purpose
# It woudl be useful to have a preparation function called by the driver

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){

  export CMSSW_RELEASE=CMSSW_11_1_0_pre8_Patatrack
  export VO_CMS_SW_DIR=/cvmfs/cms.cern.ch

  # TODO: These functions are *bad* in anything non-privileged! we can only r/w in mounted DIRs!
  cd ${resultsDir}
  source $VO_CMS_SW_DIR/cmsset_default.sh
  [[ ! -e ${CMSSW_RELEASE} ]] && scram project CMSSW ${CMSSW_RELEASE}
  cd ${CMSSW_RELEASE}/src; 
  # sets all paths to point to CMVFS; ignores system binaries
  eval `scramv1 runtime -sh`;
  cd -

  env | grep LD_LIBRARY_PATH
  env | grep SRT_LD_LIBRARY_PATH_SCRAMRT

  # TODO: This error comes from making CVMFS 555 - paths are set via <pkg>/<version>/etc/init.d/*
  export LD_LIBRARY_PATH=/bmk/cms-patatrack/CMSSW_11_1_0_pre8_Patatrack/biglib/slc7_amd64_gcc820:/bmk/cms-patatrack/CMSSW_11_1_0_pre8_Patatrack/lib/slc7_amd64_gcc820:/bmk/cms-patatrack/CMSSW_11_1_0_pre8_Patatrack/external/slc7_amd64_gcc820/lib:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_1_0_pre8_Patatrack/biglib/slc7_amd64_gcc820:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_1_0_pre8_Patatrack/lib/slc7_amd64_gcc820:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_1_0_pre8_Patatrack/external/slc7_amd64_gcc820/lib:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/llvm/9.0.1-pfdnen/lib64:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/gcc/8.2.0-bcolbf/lib64:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/gcc/8.2.0-bcolbf/lib:/usr/local/nvidia/lib:/usr/local/nvidia/lib64:/.singularity.d/libs
  
  export SRT_LD_LIBRARY_PATH_SCRAMRT=/bmk/cms-patatrack/CMSSW_11_1_0_pre8_Patatrack/biglib/slc7_amd64_gcc820:/bmk/cms-patatrack/CMSSW_11_1_0_pre8_Patatrack/lib/slc7_amd64_gcc820:/bmk/cms-patatrack/CMSSW_11_1_0_pre8_Patatrack/external/slc7_amd64_gcc820/lib:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_1_0_pre8_Patatrack/biglib/slc7_amd64_gcc820:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_1_0_pre8_Patatrack/lib/slc7_amd64_gcc820:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/cms/cmssw/CMSSW_11_1_0_pre8_Patatrack/external/slc7_amd64_gcc820/lib:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/llvm/9.0.1-pfdnen/lib64:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/gcc/8.2.0-bcolbf/lib64:/cvmfs/cms.cern.ch/slc7_amd64_gcc820/external/gcc/8.2.0-bcolbf/lib

  # Configure WL copy
  myecho "info about python and tests"
  python --version
  which python
  python -c 'import scipy; print(scipy.__path__)'
  python -c 'import numpy; print(numpy.__path__)'
  python -c 'from scipy import stats; import numpy as np; x=np.array([1,2,3]); y=np.array([1.1,2,2.9]); print(stats.linregress(x,y).slope)'
  return 0
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=8
NCOPIES=1
NEVENTS_THREAD=10
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > nproc
  NCOPIES=1
  NTHREADS=`nproc`
fi

export LC_ALL=en_US.UTF-8

# Execute common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
elif [ -f $(dirname $0)/../../../common/bmk-driver.sh ]; then
  . $(dirname $0)/../../../common/bmk-driver.sh
else
  echo "Something went wrong with the default work dir. Could not find bmk-driver.sh";
  exit 1;
fi
