#!/bin/env bash

# FIXME: THIS set of replaces should simply go in the hep-worklaods-gpu repo
# the dependency from the sciaba repo should go away, and possibly also the onte from patatrack-scripts
# or at least use a specific branch of patatrack-scripts

install_dir="/tmp/install"
echo -e "\nCloning Patatrack repos into ${install_dir}..."

ls -l ${install_dir}

cd $install_dir

# Clone software repos
git clone https://github.com/cms-patatrack/patatrack-scripts
git clone https://github.com/sciaba/patatrack-tests

echo -e "\nSet up Patatrack Scripts..."
# Prepare scripts
cp ${install_dir}/patatrack-tests/*/*.patch \
    ${install_dir}/patatrack-tests/config/sourceFromPixelRaw_cff.py \
    ${install_dir}/patatrack-scripts/

cd ${install_dir}/patatrack-scripts/
patch -b --forward workflow.sh workflow.patch

ls -l

[ ! -d /bmk/cms-patatrack ] && mkdir -p /bmk/cms-patatrack
cp -r ${install_dir}/patatrack-scripts /bmk/cms-patatrack