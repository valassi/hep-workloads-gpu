# hep-workloads-GPU

Build standalone reference HEP workloads for benchmarking purposes on GPUs

The documentation of the individual workloads can be found in the following links

| Internal Doc | Note | External Link | 
| :--- | :--- | :--- |
| [Simple Track](https://gitlab.cern.ch/hep-benchmarks/hep-workloads-gpu/-/blob/master/lhc/simpletrack/README.md)    |  Simulation of LHC turning particles | [Simpletrack github project](https://github.com/rdemaria/simpletrack) |
| [CMS Patatrack](https://gitlab.cern.ch/hep-benchmarks/hep-workloads-gpu/-/blob/master/cms/README.md)    | CMS HLT Reconstruction code | [CMS patatrack github project](https://github.com/cms-patatrack) |
